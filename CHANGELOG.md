# StrawberryFields Spring

- [2.0.0]
- [1.6.0] - 2024-08-10 - 9 files changed, 170 insertions(+), 120 deletions(-)
- [1.5.0] - 2024-03-10 - 4 files changed, 106 insertions(+), 35 deletions(-)
- [1.4.0] - 2024-02-04 - 7 files changed, 103 insertions(+), 63 deletions(-)
- [1.3.0] - 2023-11-11 - 7 files changed, 37 insertions(+), 26 deletions(-)
- [1.2.0] - 2023-11-05 - 10 files changed, 200 insertions(+), 95 deletions(-)
- [1.1.0] - 2023-08-19 - 3 files changed, 6 insertions(+), 6 deletions(-)
- 1.0.0 - 2023-08-16

[2.0.0]: https://gitlab.com/acefed/strawberryfields-spring/-/compare/9242fc89...main
[1.6.0]: https://gitlab.com/acefed/strawberryfields-spring/-/compare/297522e1...9242fc89
[1.5.0]: https://gitlab.com/acefed/strawberryfields-spring/-/compare/509a3758...297522e1
[1.4.0]: https://gitlab.com/acefed/strawberryfields-spring/-/compare/ee036e86...509a3758
[1.3.0]: https://gitlab.com/acefed/strawberryfields-spring/-/compare/29967f31...ee036e86
[1.2.0]: https://gitlab.com/acefed/strawberryfields-spring/-/compare/ce3ca3cf...29967f31
[1.1.0]: https://gitlab.com/acefed/strawberryfields-spring/-/compare/79eb76fd...ce3ca3cf

package io.gitlab.acefed.strawberryfieldsspring;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import static java.util.Map.entry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.github.cdimascio.dotenv.Dotenv;

@SpringBootApplication
@RestController
public class StrawberryfieldsSpringApplication {
    public static Dotenv dotenv = Dotenv.configure().ignoreIfMissing().load();
    public static ObjectMapper objectMapper = new ObjectMapper();
    public static String configJson;
    public static JsonNode CONFIG;
    public static String ME;
    public static RSAPrivateCrtKey PRIVATE_KEY;
    public static PublicKey PUBLIC_KEY;
    public static String publicKeyPem;

    StrawberryfieldsSpringApplication() throws Exception {
        if (dotenv.get("CONFIG_JSON") != null && !dotenv.get("CONFIG_JSON").isEmpty()) {
            configJson = dotenv.get("CONFIG_JSON");
            if (configJson.startsWith("'")) configJson = configJson.substring(1);
            if (configJson.endsWith("'")) {
                configJson = configJson.substring(0, configJson.length() - 1);
            }
        } else {
            configJson = Files.readString(Paths.get("data/config.json"));
        }
        CONFIG = objectMapper.readTree(configJson);
        ME = String.join("", List.of(
            "<a href=\"https://",
            URI.create(CONFIG.get("actor").get(0).get("me").textValue()).getHost(),
            "/\" rel=\"me nofollow noopener noreferrer\" target=\"_blank\">",
            "https://",
            URI.create(CONFIG.get("actor").get(0).get("me").textValue()).getHost(),
            "/",
            "</a>"
        ));
        PRIVATE_KEY = importPrivateKey(dotenv.get("PRIVATE_KEY"));
        PUBLIC_KEY = privateKeyToPublicKey(PRIVATE_KEY);
        publicKeyPem = exportPublicKey(PUBLIC_KEY);
    }

    public static RSAPrivateCrtKey importPrivateKey(String pem) throws Exception {
        String header = "-----BEGIN PRIVATE KEY-----";
        String footer = "-----END PRIVATE KEY-----";
        String b64 = pem;
        b64 = b64.replace("\\n", "");
        b64 = b64.replace("\n", "");
        if (b64.startsWith("\"")) b64 = b64.substring(1);
        if (b64.endsWith("\"")) b64 = b64.substring(0, b64.length() - 1);
        if (b64.startsWith(header)) b64 = b64.substring(header.length());
        if (b64.endsWith(footer)) b64 = b64.substring(0, b64.length() - footer.length());
        KeyFactory kf = KeyFactory.getInstance("RSA");
        byte[] der = Base64.getDecoder().decode(b64);
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(der);
        RSAPrivateCrtKey key = (RSAPrivateCrtKey) kf.generatePrivate(spec);
        return key;
    }

    public static PublicKey privateKeyToPublicKey(RSAPrivateCrtKey privateKey) throws Exception {
        KeyFactory kf = KeyFactory.getInstance("RSA");
        RSAPublicKeySpec spec = new RSAPublicKeySpec(privateKey.getModulus(), privateKey.getPublicExponent());
        PublicKey publicKey = kf.generatePublic(spec);
        return publicKey;
    }

    public static String exportPublicKey(PublicKey key) {
        byte[] der = key.getEncoded();
        String b64 = Base64.getEncoder().encodeToString(der);
        String pem = "-----BEGIN PUBLIC KEY-----" + "\n";
        while (b64.length() > 64) {
            pem += b64.substring(0, 64) + "\n";
            b64 = b64.substring(64);
        }
        pem += b64.substring(0, b64.length()) + "\n";
        pem += "-----END PUBLIC KEY-----" + "\n";
        return pem;
    }

    public static String uuidv7() {
        SecureRandom rand = new SecureRandom();
        byte[] v = new byte[16];
        rand.nextBytes(v);
        long ts = System.currentTimeMillis();
        v[0] = (byte) (ts >> 40);
        v[1] = (byte) (ts >> 32);
        v[2] = (byte) (ts >> 24);
        v[3] = (byte) (ts >> 16);
        v[4] = (byte) (ts >> 8);
        v[5] = (byte) ts;
        v[6] = (byte) (v[6] & 0x0F | 0x70);
        v[8] = (byte) (v[8] & 0x3F | 0x80);
        StringBuilder sb = new StringBuilder(32);
        for (byte b : v) sb.append(String.format("%02x", b));
        return sb.toString();
    }

    public static String talkScript(String req) {
        long ts = System.currentTimeMillis();
        if (URI.create(req).getHost().equals("localhost")) return "<p>" + ts + "</p>";
        return String.join("", List.of(
            "<p>",
            "<a href=\"https://",
            URI.create(req).getHost(),
            "/\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">",
            URI.create(req).getHost(),
            "</a>",
            "</p>"
        ));
    }

    public static String getActivity(String username, String hostname, String req) throws Exception {
        String t = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH)
            .withZone(ZoneId.of("GMT"))
            .format(Instant.now());
        Signature sig = Signature.getInstance("SHA256withRSA");
        sig.initSign(PRIVATE_KEY);
        sig.update(String.join("\n", List.of(
            "(request-target): get " + URI.create(req).getPath(),
            "host: " + URI.create(req).getHost(),
            "date: " + t
        )).getBytes("UTF-8"));
        String b64 = Base64.getEncoder().encodeToString(sig.sign());
        Map<String, String> headers = Map.ofEntries(
            entry("Date", t),
            entry("Signature", String.join(",", List.of(
                "keyId=\"https://" + hostname + "/u/" + username + "#Key\"",
                "algorithm=\"rsa-sha256\"",
                "headers=\"(request-target) host date\"",
                "signature=\"" + b64 + "\""
            ))),
            entry("Accept", "application/activity+json"),
            entry("Accept-Encoding", "identity"),
            entry("Cache-Control", "no-cache"),
            entry("User-Agent", "StrawberryFields-Spring/2.0.0 (+https://" + hostname + "/)")
        );
        HttpRequest.Builder builder = HttpRequest.newBuilder()
            .uri(URI.create(req))
            .GET();
        headers.forEach(builder::header);
        HttpRequest client = builder.build();
        HttpResponse<String> res = HttpClient.newHttpClient().send(client, HttpResponse.BodyHandlers.ofString());
        String status = Integer.toString(res.statusCode());
        System.out.println("GET " + req + " " + status);
        return res.body();
    }

    public static void postActivity(
        String username,
        String hostname,
        String req,
        Map<String, Object> x
    ) throws Exception {
        String t = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH)
            .withZone(ZoneId.of("GMT"))
            .format(Instant.now());
        String body = objectMapper.writeValueAsString(x);
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.update(body.getBytes("UTF-8"));
        String s256 = Base64.getEncoder().encodeToString(digest.digest());
        Signature sig = Signature.getInstance("SHA256withRSA");
        sig.initSign(PRIVATE_KEY);
        sig.update(String.join("\n", List.of(
            "(request-target): post " + URI.create(req).getPath(),
            "host: " + URI.create(req).getHost(),
            "date: " + t,
            "digest: SHA-256=" + s256
        )).getBytes("UTF-8"));
        String b64 = Base64.getEncoder().encodeToString(sig.sign());
        Map<String, String> headers = Map.ofEntries(
            entry("Date", t),
            entry("Digest", "SHA-256=" + s256),
            entry("Signature", String.join(",", List.of(
                "keyId=\"https://" + hostname + "/u/" + username + "#Key\"",
                "algorithm=\"rsa-sha256\"",
                "headers=\"(request-target) host date digest\"",
                "signature=\"" + b64 + "\""
            ))),
            entry("Accept", "application/json"),
            entry("Accept-Encoding", "gzip"),
            entry("Cache-Control", "max-age=0"),
            entry("Content-Type", "application/activity+json"),
            entry("User-Agent", "StrawberryFields-Spring/2.0.0 (+https://" + hostname + "/)")
        );
        System.out.println("POST " + req + " " + body);
        HttpRequest.Builder builder = HttpRequest.newBuilder()
            .uri(URI.create(req))
            .POST(HttpRequest.BodyPublishers.ofString(body));
        headers.forEach((k, v) -> {
            builder.header(k, v);
        });
        HttpRequest client = builder.build();
        HttpClient.newHttpClient().send(client, HttpResponse.BodyHandlers.ofString());
    }

    public static void acceptFollow(String username, String hostname, JsonNode x, JsonNode y) throws Exception {
        String aid = uuidv7();
        Map<String, Object> body = Map.ofEntries(
            entry("@context", "https://www.w3.org/ns/activitystreams"),
            entry("id", "https://" + hostname + "/u/" + username + "/s/" + aid),
            entry("type", "Accept"),
            entry("actor", "https://" + hostname + "/u/" + username),
            entry("object", y)
        );
        postActivity(username, hostname, x.get("inbox").textValue(), body);
    }

    public static void follow(String username, String hostname, JsonNode x) throws Exception {
        String aid = uuidv7();
        Map<String, Object> body = Map.ofEntries(
            entry("@context", "https://www.w3.org/ns/activitystreams"),
            entry("id", "https://" + hostname + "/u/" + username + "/s/" + aid),
            entry("type", "Follow"),
            entry("actor", "https://" + hostname + "/u/" + username),
            entry("object", x.get("id").textValue())
        );
        postActivity(username, hostname, x.get("inbox").textValue(), body);
    }

    public static void undoFollow(String username, String hostname, JsonNode x) throws Exception {
        String aid = uuidv7();
        Map<String, Object> body = Map.ofEntries(
            entry("@context", "https://www.w3.org/ns/activitystreams"),
            entry("id", "https://" + hostname + "/u/" + username + "/s/" + aid + "#Undo"),
            entry("type", "Undo"),
            entry("actor", "https://" + hostname + "/u/" + username),
            entry("object", Map.ofEntries(
                entry("id", "https://" + hostname + "/u/" + username + "/s/" + aid),
                entry("type", "Follow"),
                entry("actor", "https://" + hostname + "/u/" + username),
                entry("object", x.get("id").textValue())
            ))
        );
        postActivity(username, hostname, x.get("inbox").textValue(), body);
    }

    public static void like(String username, String hostname, JsonNode x, JsonNode y) throws Exception {
        String aid = uuidv7();
        Map<String, Object> body = Map.ofEntries(
            entry("@context", "https://www.w3.org/ns/activitystreams"),
            entry("id", "https://" + hostname + "/u/" + username + "/s/" + aid),
            entry("type", "Like"),
            entry("actor", "https://" + hostname + "/u/" + username),
            entry("object", x.get("id").textValue())
        );
        postActivity(username, hostname, y.get("inbox").textValue(), body);
    }

    public static void undoLike(String username, String hostname, JsonNode x, JsonNode y) throws Exception {
        String aid = uuidv7();
        Map<String, Object> body = Map.ofEntries(
            entry("@context", "https://www.w3.org/ns/activitystreams"),
            entry("id", "https://" + hostname + "/u/" + username + "/s/" + aid + "#Undo"),
            entry("type", "Undo"),
            entry("actor", "https://" + hostname + "/u/" + username),
            entry("object", Map.ofEntries(
                entry("id", "https://" + hostname + "/u/" + username + "/s/" + aid),
                entry("type", "Like"),
                entry("actor", "https://" + hostname + "/u/" + username),
                entry("object", x.get("id").textValue())
            ))
        );
        postActivity(username, hostname, y.get("inbox").textValue(), body);
    }

    public static void announce(String username, String hostname, JsonNode x, JsonNode y) throws Exception {
        String aid = uuidv7();
        String t = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();
        Map<String, Object> body = Map.ofEntries(
            entry("@context", "https://www.w3.org/ns/activitystreams"),
            entry("id", "https://" + hostname + "/u/" + username + "/s/" + aid + "/activity"),
            entry("type", "Announce"),
            entry("actor", "https://" + hostname + "/u/" + username),
            entry("published", t),
            entry("to", List.of("https://www.w3.org/ns/activitystreams#Public")),
            entry("cc", List.of("https://" + hostname + "/u/" + username + "/followers")),
            entry("object", x.get("id").textValue())
        );
        postActivity(username, hostname, y.get("inbox").textValue(), body);
    }

    public static void undoAnnounce(
        String username,
        String hostname,
        JsonNode x,
        JsonNode y,
        String z
    ) throws Exception {
        String aid = uuidv7();
        Map<String, Object> body = Map.ofEntries(
            entry("@context", "https://www.w3.org/ns/activitystreams"),
            entry("id", "https://" + hostname + "/u/" + username + "/s/" + aid + "#Undo"),
            entry("type", "Undo"),
            entry("actor", "https://" + hostname + "/u/" + username),
            entry("object", Map.ofEntries(
                entry("id", z + "/activity"),
                entry("type", "Announce"),
                entry("actor", "https://" + hostname + "/u/" + username),
                entry("to", List.of("https://www.w3.org/ns/activitystreams#Public")),
                entry("cc", List.of("https://" + hostname + "/u/" + username + "/followers")),
                entry("object", x.get("id").textValue())
            ))
        );
        postActivity(username, hostname, y.get("inbox").textValue(), body);
    }

    public static void createNote(String username, String hostname, JsonNode x, String y) throws Exception {
        String aid = uuidv7();
        String t = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();
        Map<String, Object> body = Map.ofEntries(
            entry("@context", "https://www.w3.org/ns/activitystreams"),
            entry("id", "https://" + hostname + "/u/" + username + "/s/" + aid + "/activity"),
            entry("type", "Create"),
            entry("actor", "https://" + hostname + "/u/" + username),
            entry("published", t),
            entry("to", List.of("https://www.w3.org/ns/activitystreams#Public")),
            entry("cc", List.of("https://" + hostname + "/u/" + username + "/followers")),
            entry("object", Map.ofEntries(
                entry("id", "https://" + hostname + "/u/" + username + "/s/" + aid),
                entry("type", "Note"),
                entry("attributedTo", "https://" + hostname + "/u/" + username),
                entry("content", talkScript(y)),
                entry("url", "https://" + hostname + "/u/" + username + "/s/" + aid),
                entry("published", t),
                entry("to", List.of("https://www.w3.org/ns/activitystreams#Public")),
                entry("cc", List.of("https://" + hostname + "/u/" + username + "/followers"))
            ))
        );
        postActivity(username, hostname, x.get("inbox").textValue(), body);
    }

    public static void createNoteImage(
        String username,
        String hostname,
        JsonNode x,
        String y,
        String z
    ) throws Exception {
        String aid = uuidv7();
        String t = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();
        Map<String, Object> body = Map.ofEntries(
            entry("@context", "https://www.w3.org/ns/activitystreams"),
            entry("id", "https://" + hostname + "/u/" + username + "/s/" + aid + "/activity"),
            entry("type", "Create"),
            entry("actor", "https://" + hostname + "/u/" + username),
            entry("published", t),
            entry("to", List.of("https://www.w3.org/ns/activitystreams#Public")),
            entry("cc", List.of("https://" + hostname + "/u/" + username + "/followers")),
            entry("object", Map.ofEntries(
                entry("id", "https://" + hostname + "/u/" + username + "/s/" + aid),
                entry("type", "Note"),
                entry("attributedTo", "https://" + hostname + "/u/" + username),
                entry("content", talkScript("https://localhost")),
                entry("url", "https://" + hostname + "/u/" + username + "/s/" + aid),
                entry("published", t),
                entry("to", List.of("https://www.w3.org/ns/activitystreams#Public")),
                entry("cc", List.of("https://" + hostname + "/u/" + username + "/followers")),
                entry("attachment", List.of(
                    Map.ofEntries(
                        entry("type", "Image"),
                        entry("mediaType", z),
                        entry("url", y)
                    )
                ))
            ))
        );
        postActivity(username, hostname, x.get("inbox").textValue(), body);
    }

    public static void createNoteMention(
        String username,
        String hostname,
        JsonNode x,
        JsonNode y,
        String z
    ) throws Exception {
        String aid = uuidv7();
        String t = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();
        String at =
            "@" + y.get("preferredUsername").textValue() + "@" + URI.create(y.get("inbox").textValue()).getHost();
        Map<String, Object> body = Map.ofEntries(
            entry("@context", "https://www.w3.org/ns/activitystreams"),
            entry("id", "https://" + hostname + "/u/" + username + "/s/" + aid + "/activity"),
            entry("type", "Create"),
            entry("actor", "https://" + hostname + "/u/" + username),
            entry("published", t),
            entry("to", List.of("https://www.w3.org/ns/activitystreams#Public")),
            entry("cc", List.of("https://" + hostname + "/u/" + username + "/followers")),
            entry("object", Map.ofEntries(
                entry("id", "https://" + hostname + "/u/" + username + "/s/" + aid),
                entry("type", "Note"),
                entry("attributedTo", "https://" + hostname + "/u/" + username),
                entry("inReplyTo", x.get("id").textValue()),
                entry("content", talkScript(z)),
                entry("url", "https://" + hostname + "/u/" + username + "/s/" + aid),
                entry("published", t),
                entry("to", List.of("https://www.w3.org/ns/activitystreams#Public")),
                entry("cc", List.of("https://" + hostname + "/u/" + username + "/followers")),
                entry("tag", List.of(
                    Map.ofEntries(
                        entry("type", "Mention"),
                        entry("name", at)
                    )
                ))
            ))
        );
        postActivity(username, hostname, y.get("inbox").textValue(), body);
    }

    public static void createNoteHashtag(
        String username,
        String hostname,
        JsonNode x,
        String y,
        String z
    ) throws Exception {
        String aid = uuidv7();
        String t = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();
        Map<String, Object> body = Map.ofEntries(
            entry("@context", List.of(
                "https://www.w3.org/ns/activitystreams",
                Map.ofEntries(entry("Hashtag", "as:Hashtag"))
            )),
            entry("id", "https://" + hostname + "/u/" + username + "/s/" + aid + "/activity"),
            entry("type", "Create"),
            entry("actor", "https://" + hostname + "/u/" + username),
            entry("published", t),
            entry("to", List.of("https://www.w3.org/ns/activitystreams#Public")),
            entry("cc", List.of("https://" + hostname + "/u/" + username + "/followers")),
            entry("object", Map.ofEntries(
                entry("id", "https://" + hostname + "/u/" + username + "/s/" + aid),
                entry("type", "Note"),
                entry("attributedTo", "https://" + hostname + "/u/" + username),
                entry("content", talkScript(y)),
                entry("url", "https://" + hostname + "/u/" + username + "/s/" + aid),
                entry("published", t),
                entry("to", List.of("https://www.w3.org/ns/activitystreams#Public")),
                entry("cc", List.of("https://" + hostname + "/u/" + username + "/followers")),
                entry("tag", List.of(
                    Map.ofEntries(
                        entry("type", "Hashtag"),
                        entry("name", "#" + z )
                    )
                ))
            ))
        );
        postActivity(username, hostname, x.get("inbox").textValue(), body);
    }

    public static void updateNote(String username, String hostname, JsonNode x, String y) throws Exception {
        String t = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();
        long ts = System.currentTimeMillis();
        Map<String, Object> body = Map.ofEntries(
            entry("@context", "https://www.w3.org/ns/activitystreams"),
            entry("id", y + "#" + ts),
            entry("type", "Update"),
            entry("actor", "https://" + hostname + "/u/" + username),
            entry("published", t),
            entry("to", List.of("https://www.w3.org/ns/activitystreams#Public")),
            entry("cc", List.of("https://" + hostname + "/u/" + username + "/followers")),
            entry("object", Map.ofEntries(
                entry("id", y),
                entry("type", "Note"),
                entry("attributedTo", "https://" + hostname + "/u/" + username),
                entry("content", talkScript("https://localhost")),
                entry("url", y),
                entry("updated", t),
                entry("to", List.of("https://www.w3.org/ns/activitystreams#Public")),
                entry("cc", List.of("https://" + hostname + "/u/" + username + "/followers"))
            ))
        );
        postActivity(username, hostname, x.get("inbox").textValue(), body);
    }

    public static void deleteTombstone(String username, String hostname, JsonNode x, String y) throws Exception {
        Map<String, Object> body = Map.ofEntries(
            entry("@context", "https://www.w3.org/ns/activitystreams"),
            entry("id", y + "#Delete"),
            entry("type", "Delete"),
            entry("actor", "https://" + hostname + "/u/" + username),
            entry("object", Map.ofEntries(
                entry("id", y),
                entry("type", "Tombstone")
            ))
        );
        postActivity(username, hostname, x.get("inbox").textValue(), body);
    }

    public static void main(String[] args) {
        SpringApplication.run(StrawberryfieldsSpringApplication.class, args);
    }

    @GetMapping("/")
    public static ResponseEntity<String> home() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "text/plain");
        return new ResponseEntity<>("StrawberryFields Spring", headers, HttpStatus.OK);
    }

    @GetMapping("/about")
    public static ResponseEntity<String> about() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "text/plain");
        return new ResponseEntity<>("About: Blank", headers, HttpStatus.OK);
    }

    @GetMapping("/u/{username}")
    public static ResponseEntity<Object> uUser(@PathVariable String username, @RequestHeader HttpHeaders req) {
        String hostname = URI.create(CONFIG.get("origin").textValue()).getHost();
        String acceptHeaderField = req.getAccept().toString();
        Boolean hasType = false;
        if (!username.equals(CONFIG.get("actor").get(0).get("preferredUsername").textValue())) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        if (acceptHeaderField.contains("application/activity+json")) hasType = true;
        if (acceptHeaderField.contains("application/ld+json")) hasType = true;
        if (acceptHeaderField.contains("application/json")) hasType = true;
        if (!hasType) {
            String body = username + ": " + CONFIG.get("actor").get(0).get("name").textValue();
            Map<String, String> headerMap = Map.ofEntries(
                entry("Cache-Control", "public, max-age=" + CONFIG.get("ttl").asText() + ", must-revalidate"),
                entry("Vary", "Accept, Accept-Encoding"),
                entry("Content-Type", "text/plain")
            );
            HttpHeaders headers = new HttpHeaders();
            headerMap.forEach(headers::set);
            return new ResponseEntity<>(body, headers, HttpStatus.OK);
        }
        Map<String, Object> body = Map.ofEntries(
            entry("@context", List.of(
                "https://www.w3.org/ns/activitystreams",
                "https://w3id.org/security/v1",
                Map.ofEntries(
                    entry("schema", "https://schema.org/"),
                    entry("PropertyValue", "schema:PropertyValue"),
                    entry("value", "schema:value"),
                    entry("Key", "sec:Key")
                )
            )),
            entry("id", "https://" + hostname + "/u/" + username),
            entry("type", "Person"),
            entry("inbox", "https://" + hostname + "/u/" + username + "/inbox"),
            entry("outbox", "https://" + hostname + "/u/" + username + "/outbox"),
            entry("following", "https://" + hostname + "/u/" + username + "/following"),
            entry("followers", "https://" + hostname + "/u/" + username + "/followers"),
            entry("preferredUsername", username),
            entry("name", CONFIG.get("actor").get(0).get("name").textValue()),
            entry("summary", "<p>2.0.0</p>"),
            entry("url", "https://" + hostname + "/u/" + username),
            entry("endpoints", Map.ofEntries(entry("sharedInbox", "https://" + hostname + "/u/" + username + "/inbox"))),
            entry("attachment", List.of(
                Map.ofEntries(
                    entry("type", "PropertyValue"),
                    entry("name", "me"),
                    entry("value", ME)
                )
            )),
            entry("icon", Map.ofEntries(
                entry("type", "Image"),
                entry("mediaType", "image/png"),
                entry("url", "https://" + hostname + "/static/" + username + "u.png")
            )),
            entry("image", Map.ofEntries(
                entry("type", "Image"),
                entry("mediaType", "image/png"),
                entry("url", "https://" + hostname + "/static/" + username + "s.png")
            )),
            entry("publicKey", Map.ofEntries(
                entry("id", "https://" + hostname + "/u/" + username + "#Key"),
                entry("type", "Key"),
                entry("owner", "https://" + hostname + "/u/" + username),
                entry("publicKeyPem", publicKeyPem)
            ))
        );
        Map<String, String> headerMap = Map.ofEntries(
            entry("Cache-Control", "public, max-age=" + CONFIG.get("ttl").asText() + ", must-revalidate"),
            entry("Vary", "Accept, Accept-Encoding"),
            entry("Content-Type", "application/activity+json")
        );
        HttpHeaders headers = new HttpHeaders();
        headerMap.forEach(headers::set);
        return new ResponseEntity<>(body, headers, HttpStatus.OK);
    }

    @GetMapping("/u/{username}/inbox")
    public static void getInbox(@PathVariable String username) {
        throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED);
    }
    @PostMapping("/u/{username}/inbox")
    public static String postInbox(
        @PathVariable String username,
        @RequestBody String y,
        @RequestHeader HttpHeaders req
    ) throws Exception {
        String hostname = URI.create(CONFIG.get("origin").textValue()).getHost();
        String contentTypeHeaderField = req.getContentType().toString();
        Boolean hasType = false;
        JsonNode ny = objectMapper.readTree(y);
        String t = Objects.toString(ny.path("type").textValue(), "");
        String aid = Objects.toString(ny.path("id").textValue(), "");
        String atype = Objects.toString(ny.path("type").textValue(), "");
        if (aid.length() > 1024 || atype.length() > 64) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        System.out.println("INBOX " + aid + " " + atype);
        if (!username.equals(CONFIG.get("actor").get(0).get("preferredUsername").textValue())) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        if (contentTypeHeaderField.contains("application/activity+json")) hasType = true;
        if (contentTypeHeaderField.contains("application/ld+json")) hasType = true;
        if (contentTypeHeaderField.contains("application/json")) hasType = true;
        if (!hasType) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if (req.get("Digest") == null || req.get("Digest").isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        if (req.get("Signature") == null || req.get("Signature").isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        if (t.equals("Accept") || t.equals("Reject") || t.equals("Add")) return null;
        if (t.equals("Remove") || t.equals("Like") || t.equals("Announce")) return null;
        if (t.equals("Create") || t.equals("Update") || t.equals("Delete")) return null;
        if (t.equals("Follow")) {
            if (!URI.create(Objects.toString(ny.path("actor").textValue(), "about:blank"))
                    .getScheme()
                    .equals("https")) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }
            String x = getActivity(username, hostname, ny.get("actor").textValue());
            if (x == null || x.isEmpty()) throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
            JsonNode nx = objectMapper.readTree(x);
            acceptFollow(username, hostname, nx, ny);
            return null;
        }
        if (t.equals("Undo")) {
            ObjectNode nz = ny.withObject("object");
            t = Objects.toString(nz.path("type").textValue(), "");
            if (t.equals("Accept") || t.equals("Like") || t.equals("Announce")) return null;
            if (t.equals("Follow")) {
                if (!URI.create(Objects.toString(ny.path("actor").textValue(), "about:blank"))
                        .getScheme()
                        .equals("https")) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
                }
                String x = getActivity(username, hostname, ny.get("actor").textValue());
                if (x == null || x.isEmpty()) throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
                JsonNode nx = objectMapper.readTree(x);
                acceptFollow(username, hostname, nx, nz);
                return null;
            }
        }
        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PostMapping("/u/{username}/outbox")
    public static void postOutbox(@PathVariable String username) {
        throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED);
    }
    @GetMapping("/u/{username}/outbox")
    public static ResponseEntity<Map> getOutbox(@PathVariable String username, @RequestHeader HttpHeaders req) {
        String hostname = URI.create(CONFIG.get("origin").textValue()).getHost();
        if (!username.equals(CONFIG.get("actor").get(0).get("preferredUsername").textValue())) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        Map<String, Object> body = Map.ofEntries(
            entry("@context", "https://www.w3.org/ns/activitystreams"),
            entry("id", "https://" + hostname + "/u/" + username + "/outbox"),
            entry("type", "OrderedCollection"),
            entry("totalItems", 0)
        );
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/activity+json");
        return new ResponseEntity<>(body, headers, HttpStatus.OK);
    }

    @GetMapping("/u/{username}/following")
    public static ResponseEntity<Map> following(@PathVariable String username, @RequestHeader HttpHeaders req) {
        String hostname = URI.create(CONFIG.get("origin").textValue()).getHost();
        if (!username.equals(CONFIG.get("actor").get(0).get("preferredUsername").textValue())) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        Map<String, Object> body = Map.ofEntries(
            entry("@context", "https://www.w3.org/ns/activitystreams"),
            entry("id", "https://" + hostname + "/u/" + username + "/following"),
            entry("type", "OrderedCollection"),
            entry("totalItems", 0)
        );
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/activity+json");
        return new ResponseEntity<>(body, headers, HttpStatus.OK);
    }

    @GetMapping("/u/{username}/followers")
    public static ResponseEntity<Map> followers(@PathVariable String username, @RequestHeader HttpHeaders req) {
        String hostname = URI.create(CONFIG.get("origin").textValue()).getHost();
        if (!username.equals(CONFIG.get("actor").get(0).get("preferredUsername").textValue())) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        Map<String, Object> body = Map.ofEntries(
            entry("@context", "https://www.w3.org/ns/activitystreams"),
            entry("id", "https://" + hostname + "/u/" + username + "/followers"),
            entry("type", "OrderedCollection"),
            entry("totalItems", 0)
        );
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/activity+json");
        return new ResponseEntity<>(body, headers, HttpStatus.OK);
    }

    @PostMapping("/s/{secret}/u/{username}")
    public static String sSend(
        @PathVariable String username,
        @PathVariable String secret,
        @RequestBody String body,
        @RequestHeader HttpHeaders req
    ) throws Exception {
        String hostname = URI.create(CONFIG.get("origin").textValue()).getHost();
        JsonNode send = objectMapper.readTree(body);
        String t = Objects.toString(send.path("type").textValue(), "");
        if (!username.equals(CONFIG.get("actor").get(0).get("preferredUsername").textValue())) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        if (secret == null || secret.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        if (secret.equals("-")) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        if (!secret.equals(dotenv.get("SECRET"))) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        if (!URI.create(Objects.toString(send.path("id").textValue(), "about:blank")).getScheme().equals("https")) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        String x = getActivity(username, hostname, Objects.toString(send.get("id").textValue(), ""));
        if (x == null || x.isEmpty()) throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        JsonNode nx = objectMapper.readTree(x);
        String aid = Objects.toString(nx.path("id").textValue(), "");
        String atype = Objects.toString(nx.path("type").textValue(), "");
        if (aid.length() > 1024 || atype.length() > 64) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        if (t.equals("follow")) {
            follow(username, hostname, nx);
            return null;
        }
        if (t.equals("undo_follow")) {
            undoFollow(username, hostname, nx);
            return null;
        }
        if (t.equals("like")) {
            if (!URI.create(Objects.toString(nx.path("attributedTo").textValue(), "about:blank"))
                    .getScheme()
                    .equals("https")) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }
            String y = getActivity(username, hostname, nx.get("attributedTo").textValue());
            if (y == null || y.isEmpty()) throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
            JsonNode ny = objectMapper.readTree(y);
            like(username, hostname, nx, ny);
            return null;
        }
        if (t.equals("undo_like")) {
            if (!URI.create(Objects.toString(nx.path("attributedTo").textValue(), "about:blank"))
                    .getScheme()
                    .equals("https")) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }
            String y = getActivity(username, hostname, nx.get("attributedTo").textValue());
            if (y == null || y.isEmpty()) throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
            JsonNode ny = objectMapper.readTree(y);
            undoLike(username, hostname, nx, ny);
            return null;
        }
        if (t.equals("announce")) {
            if (!URI.create(Objects.toString(nx.path("attributedTo").textValue(), "about:blank"))
                    .getScheme()
                    .equals("https")) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }
            String y = getActivity(username, hostname, nx.get("attributedTo").textValue());
            if (y == null || y.isEmpty()) throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
            JsonNode ny = objectMapper.readTree(y);
            announce(username, hostname, nx, ny);
            return null;
        }
        if (t.equals("undo_announce")) {
            if (!URI.create(Objects.toString(nx.path("attributedTo").textValue(), "about:blank"))
                    .getScheme()
                    .equals("https")) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }
            String y = getActivity(username, hostname, nx.get("attributedTo").textValue());
            if (y == null || y.isEmpty()) throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
            JsonNode ny = objectMapper.readTree(y);
            String z;
            if (send.hasNonNull("url") && !send.path("url").textValue().isEmpty()) {
                z = send.get("url").textValue();
            } else {
                z = "https://" + hostname + "/u/" + username + "/s/00000000000000000000000000000000";
            }
            if (!URI.create(z).getScheme().equals("https")) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            undoAnnounce(username, hostname, nx, ny, z);
            return null;
        }
        if (t.equals("create_note")) {
            String y;
            if (send.hasNonNull("url") && !send.path("url").textValue().isEmpty()) {
                y = send.get("url").textValue();
            } else {
                y = "https://localhost";
            }
            if (!URI.create(y).getScheme().equals("https")) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            createNote(username, hostname, nx, y);
            return null;
        }
        if (t.equals("create_note_image")) {
            String y;
            if (send.hasNonNull("url") && !send.path("url").textValue().isEmpty()) {
                y = send.get("url").textValue();
            } else {
                y = "https://" + hostname + "/static/logo.png";
            }
            if (!URI.create(y).getScheme().equals("https") || !URI.create(y).getHost().equals(hostname)) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }
            String z = "image/png";
            if (y.endsWith(".jpg") || y.endsWith(".jpeg")) z = "image/jpeg";
            if (y.endsWith(".svg")) z = "image/svg+xml";
            if (y.endsWith(".gif")) z = "image/gif";
            if (y.endsWith(".webp")) z = "image/webp";
            if (y.endsWith(".avif")) z = "image/avif";
            createNoteImage(username, hostname, nx, y, z);
            return null;
        }
        if (t.equals("create_note_mention")) {
            if (!URI.create(Objects.toString(nx.path("attributedTo").textValue(), "about:blank"))
                    .getScheme()
                    .equals("https")) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }
            String y = getActivity(username, hostname, nx.get("attributedTo").textValue());
            if (y == null || y.isEmpty()) throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
            JsonNode ny = objectMapper.readTree(y);
            String z;
            if (send.hasNonNull("url") && !send.path("url").textValue().isEmpty()) {
                z = send.get("url").textValue();
            } else {
                z = "https://localhost";
            }
            if (!URI.create(z).getScheme().equals("https")) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            createNoteMention(username, hostname, nx, ny, z);
            return null;
        }
        if (t.equals("create_note_hashtag")) {
            String y, z;
            if (send.hasNonNull("url") && !send.path("url").textValue().isEmpty()) {
                y = send.get("url").textValue();
            } else {
                y = "https://localhost";
            }
            if (!URI.create(y).getScheme().equals("https")) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            if (send.hasNonNull("tag") && !send.path("tag").textValue().isEmpty()) {
                z = send.get("url").textValue();
            } else {
                z = "Hashtag";
            }
            createNoteHashtag(username, hostname, nx, y, z);
            return null;
        }
        if (t.equals("update_note")) {
            String y;
            if (send.hasNonNull("url") && !send.path("url").textValue().isEmpty()) {
                y = send.get("url").textValue();
            } else {
                y = "https://" + hostname + "/u/" + username + "/s/00000000000000000000000000000000";
            }
            if (!URI.create(y).getScheme().equals("https")) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            updateNote(username, hostname, nx, y);
            return null;
        }
        if (t.equals("delete_tombstone")) {
            String y;
            if (send.hasNonNull("url") && !send.path("url").textValue().isEmpty()) {
                y = send.get("url").textValue();
            } else {
                y = "https://" + hostname + "/u/" + username + "/s/00000000000000000000000000000000";
            }
            if (!URI.create(y).getScheme().equals("https")) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            deleteTombstone(username, hostname, nx, y);
            return null;
        }
        System.out.println("TYPE " + aid + " " + atype);
        return null;
    }

    @GetMapping("/.well-known/nodeinfo")
    public static ResponseEntity<Map> nodeinfo(@RequestHeader HttpHeaders req) {
        String hostname = URI.create(CONFIG.get("origin").textValue()).getHost();
        Map<String, Object> body = Map.ofEntries(
            entry("links", List.of(
                Map.ofEntries(
                    entry("rel", "http://nodeinfo.diaspora.software/ns/schema/2.0"),
                    entry("href", "https://" + hostname + "/nodeinfo/2.0.json")
                ),
                Map.ofEntries(
                    entry("rel", "http://nodeinfo.diaspora.software/ns/schema/2.1"),
                    entry("href", "https://" + hostname + "/nodeinfo/2.1.json")
                )
            ))
        );
        Map<String, String> headerMap = Map.ofEntries(
            entry("Cache-Control", "public, max-age=" + CONFIG.get("ttl").asText() + ", must-revalidate"),
            entry("Vary", "Accept, Accept-Encoding")
        );
        HttpHeaders headers = new HttpHeaders();
        headerMap.forEach(headers::set);
        return new ResponseEntity<>(body, headers, HttpStatus.OK);
    }

    @GetMapping("/.well-known/webfinger")
    public static ResponseEntity<Map> webfinger(@RequestParam String resource, @RequestHeader HttpHeaders req) {
        String username = CONFIG.get("actor").get(0).get("preferredUsername").textValue();
        String hostname = URI.create(CONFIG.get("origin").textValue()).getHost();
        String p443 = "https://" + hostname + ":443/";
        Boolean hasResource = false;
        if (resource.startsWith(p443)) {
            resource = "https://" + hostname + "/" + resource.substring(p443.length());
        }
        if (resource.equals("acct:" + username + "@" + hostname)) hasResource = true;
        if (resource.equals("mailto:" + username + "@" + hostname)) hasResource = true;
        if (resource.equals("https://" + hostname + "/@" + username)) hasResource = true;
        if (resource.equals("https://" + hostname + "/u/" + username)) hasResource = true;
        if (resource.equals("https://" + hostname + "/user/" + username)) hasResource = true;
        if (resource.equals("https://" + hostname + "/users/" + username)) hasResource = true;
        if (!hasResource) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        Map<String, Object> body = Map.ofEntries(
            entry("subject", "acct:" + username + "@" + hostname),
            entry("aliases", List.of(
                "mailto:" + username + "@" + hostname,
                "https://" + hostname + "/@" + username,
                "https://" + hostname + "/u/" + username,
                "https://" + hostname + "/user/" + username,
                "https://" + hostname + "/users/" + username
            )),
            entry("links", List.of(
                Map.ofEntries(
                    entry("rel", "self"),
                    entry("type", "application/activity+json"),
                    entry("href", "https://" + hostname + "/u/" + username)
                ),
                Map.ofEntries(
                    entry("rel", "http://webfinger.net/rel/avatar"),
                    entry("type", "image/png"),
                    entry("href", "https://" + hostname + "/static/" + username + "u.png")
                ),
                Map.ofEntries(
                    entry("rel", "http://webfinger.net/rel/profile-page"),
                    entry("type", "text/plain"),
                    entry("href", "https://" + hostname + "/u/" + username)
                )
            ))
        );
        Map<String, String> headerMap = Map.ofEntries(
            entry("Cache-Control", "public, max-age=" + CONFIG.get("ttl").asText() + ", must-revalidate"),
            entry("Vary", "Accept, Accept-Encoding"),
            entry("Content-Type", "application/jrd+json")
        );
        HttpHeaders headers = new HttpHeaders();
        headerMap.forEach(headers::set);
        return new ResponseEntity<>(body, headers, HttpStatus.OK);
    }

    @GetMapping({ "/@", "/u", "/user", "/users" })
    public static ResponseEntity<String> u() {
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create("/"));
        return new ResponseEntity<>(null, headers, HttpStatus.FOUND);
    }

    @GetMapping({ "/users/{username}", "/user/{username}", "/@{username}" })
    public static ResponseEntity<String> uu(@PathVariable String username) {
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create("/u/" + username));
        return new ResponseEntity<>(null, headers, HttpStatus.FOUND);
    }
}
